function clearSessionStorage() {
  let storedKeys = keys(sessionStorage).filter(key => key.startsWith('MnM'));
  storedKeys.forEach(key => {
    sessionStorage.removeItem(key);
  });
  return;
}

function mnmRemoveIgnored(){
  const ignoreClasses = ['phantom','ignored'];
  let ignoredIds = [];
  let sessionId = `MnMignoredIds${tabId}`;
  let ignoredComments = document.getElementsByClassName(...ignoreClasses);
  const storedIdx = sessionStorage.getItem(sessionId);
  if(storedIdx === null){
    window.addEventListener("beforeunload", clearSessionStorage);
    sessionStorage.setItem(sessionId, '');
  }else {
    if(storedIdx.length){
      if(storedIdx.indexOf(',') !== -1){
        ignoredIds = storedIdx.split(',');
      }else{
        ignoredIds = [storedIdx];
      }
    }
  }
  while(ignoredComments.length) {
    ignoredComments = document.getElementsByClassName(...ignoreClasses);
    for (let comment of ignoredComments) {
      if(comment.hasAttribute('id')){
        ignoredIds.push(comment.id);
      }
      comment.classList.remove(...ignoreClasses);
    }
  }
  let ignoredIdesCleaned = [...new Set(ignoredIds)];
  sessionStorage.setItem(sessionId, ignoredIdesCleaned);
}
mnmRemoveIgnored();