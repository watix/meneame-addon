let currentTabId = 0;
let contextMenu = {};
const menuId = "mnm-ignore-id";

function updateMenu() {
  let sessionId = `MnMmenucheck${currentTabId}`;
  let checkMenu = contextMenu[sessionId];
  if(!checkMenu){
    contextMenu[sessionId] = false;
  }
  let titleTxt = contextMenu[sessionId] ? 'Hide' : 'show'
  browser.contextMenus.update(menuId, {title: `${titleTxt} igonerd`,checked: contextMenu[sessionId]});
  browser.contextMenus.refresh();
}
browser.contextMenus.create({
  id: menuId,
  type: "checkbox",
  title: `Show igonerd`,
  documentUrlPatterns: ["*://*.meneame.net/m/*", "*://*.meneame.net/story/*"],
  contexts: ["page"],
  checked: false
});

browser.contextMenus.onShown.addListener(function(info, tab) {
  currentTabId = tab.id;
  updateMenu();
})

browser.contextMenus.onClicked.addListener(function(info, tab) {
  let sessionId = `MnMmenucheck${tab.id}`;
  if (info.menuItemId == menuId) {
    if(info.checked && !info.wasChecked){
      contextMenu[sessionId] = true;
      browser.tabs.executeScript(tab.id, {
        code: `var tabId = ${tab.id};` 
      }, function() {
        browser.tabs.executeScript(tab.id, {
          file: '/showIgnored.js'
        });
      });
    }
    if(!info.checked && info.wasChecked){
      contextMenu[sessionId] = false;
      browser.tabs.executeScript(tab.id, {
        code: `var tabId = ${tab.id};` 
      }, function() {
        browser.tabs.executeScript(tab.id, {
          file: '/hideIgnored.js'
        });
      });
    }
  }
});