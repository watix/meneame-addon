function mnmAddIgnored(){
  const ignoreClasses = ['phantom','ignored'];
  let ignoredIds = [];
  let sessionId = `MnMignoredIds${tabId}`;
  const storedIdx =  sessionStorage.getItem(sessionId);
  if(storedIdx){
    if(storedIdx.length){
      if(storedIdx.indexOf(',') !== -1){
        ignoredIds = storedIdx.split(',');
      }else{
        ignoredIds = [storedIdx];
      }
    }
  }
  while(ignoredIds.length) {
    for (let cid of ignoredIds) {
      ignoredIds.splice(ignoredIds.indexOf(cid),1);
      document.getElementById(cid).classList.add(...ignoreClasses);
    }
  }
  sessionStorage.setItem(sessionId, '');
}
mnmAddIgnored();